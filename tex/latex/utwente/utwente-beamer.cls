\RequirePackage{ expl3, l3keys2e, xparse, ragged2e }
\ProvidesExplClass
	{utwente-beamer}
	{2017/08/23}
	{1.1}
	{University of Twente document class for presentations}

% Package options
\keys_define:nn { utwente_beamer } {
	% Token lists
	background  .tl_set:N = \utwente_beamer_background,
	banner      .tl_set:N = \utwente_beamer_banner,
	language    .tl_set:N = \utwente_beamer_language,

	% Switches
	outline     .bool_set:N         = \utwente_beamer_outline,
	nooutline   .bool_set_inverse:N = \utwente_beamer_outline,

	% Passthrough
	type1       .code:n = \PassOptionsToPackage{type1}{utwentefont},
	otf         .code:n = \PassOptionsToPackage{otf}{utwentefont},
	math        .code:n = \PassOptionsToPackage{math}{utwentefont},
	freefont    .code:n = \PassOptionsToPackage{freefont}{utwentefont},

	% Meta
	english     .meta:n = { language = en },
	dutch       .meta:n = { language = nl },
	t1          .meta:n = { type1 },
	opentype    .meta:n = { otf },

	% Shorthand for widescreen slides
	wide        .meta:n = { aspectratio = 169 },

	% Options for beamer
	unknown    .code:n =
		\PassOptionsToClass{ \l_keys_key_tl = #1 }{ beamer },
}

% Default options
\keys_set:nn { utwente_beamer } {
	language = nl,
	background = 1,
	banner = 1,
	outline
}
\ProcessKeysOptions{ utwente_beamer }

% Load more colours
\PassOptionsToClass{ xcolor={usenames,dvipsnames,table,svgnames} }{ beamer }

% Load the beamer class
\LoadClass{beamer}

% Load UTwente fonts
\RequirePackage{utwentefont}

% Variables
\cs_set:Npn \setlanguage   #1 { \tl_set:Nn \utwente_beamer_language {#1} }
\cs_new:Npn \setbackground #1 { \tl_set:Nn \utwente_beamer_background {#1} }
\cs_new:Npn \setbanner     #1 {
    \tl_set:Nn \utwente_beamer_banner {#1}
    \utwente_beamer_banner_set
}

% Margins
\setbeamersize{ text~margin~left = 16mm, text~margin~right = 5mm }

% Styling
\setbeamercolor { normal~text } { fg=black }
\setbeamercolor { structure }   { fg=black }

% Show a table of contents at the start of a section
\bool_if:NT \utwente_beamer_outline
	{ \AtBeginSection[] { \contentsslide } }

% Frame title
\setbeamertemplate{frametitle}{
	\vspace{1ex}
	\vbox:n { \universtitle \MakeUppercase \insertframetitle }
	\vspace{.5ex}
	\hrule
}

% Footer
\setbeamertemplate{footline} {
	% Use title font
	\universtitle

	% UTwente logo
	\hspace{16mm}
	\includegraphics
	    [ height=1mm, trim=7mm~7mm~7mm~7mm ]
	    { utwente-\utwente_beamer_language-black }

	% Buffer space
	\hspace{1em}

	% Place short title in the centre
	\hbox_to_wd:nn{.5\paperwidth}{
	    \hfill
	    \MakeUppercase{\insertshorttitle} % This doesn't work
	    \hfill
	}

	% Flush the rest to the right
	\hfill

	% Date / slide number
	\insertdate
	\quad
	\insertframenumber{} / \inserttotalframenumber

	% Right margin
	\hspace{5mm}

	% Text vertical offset
	\vspace{2mm}
}

% Set the banner
\tl_set:Nn \utwente_beamer_banner_set {
	\setbeamertemplate {background} {
	    \includegraphics
	        [ width=14mm,height=\paperheight ]
	        { utwente-beamer-banner-\utwente_beamer_banner }
	}
}

% Clear the banner
\tl_set:Nn \utwente_beamer_banner_clear {
	\setbeamertemplate { background } {}
}

% Immediately set it
\utwente_beamer_banner_set

% Aliasses for users
\tl_set_eq:NN \clearbanner \utwente_beamer_banner_clear

% Title slide
\tl_set:Nn \maketitleslide {
	% Clear the banner
	\utwente_beamer_banner_clear

	% Set the background
	\usebackgroundtemplate {
	    \includegraphics
	        [ width=\paperwidth ]
	        { utwente-beamer-back-\utwente_beamer_language-\utwente_beamer_background }
	}

	% Slide contents
	\begin{frame}<beamer>[plain]
	    \universtitle
	    \color{white}
		\hspace{20mm}
	    \parbox { .6\paperwidth } {
	        { \Large  \MakeUppercase \inserttitle } \\
	        { \insertsubtitle }
	    }
	\end{frame}

	% Clear the background
	\usebackgroundtemplate{}

	% Reset the banner
	\utwente_beamer_banner_set
}

% Slide with the table of contents
\tl_set:Nn \contentsslide {
   \begin{frame} <beamer> {Contents}
	   \tableofcontents[currentsection,currentsubsection]
   \end{frame}
}
